package com.zmh.wt.healthmonitor.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zmh.wt.healthmonitor.util.DateUtil;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

/**
 * @Author MH.Zhang
 * @Classname MyMetaObjectHandler
 * @Date 2023/2/22 21:59
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 插入时自动填充
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {

        this.setFieldValByName("gmtModified", DateUtil.currentDateTime(), metaObject);
        this.setFieldValByName("gmtCreate", DateUtil.currentDateTime(), metaObject);
        this.setFieldValByName("gmtReceived", DateUtil.currentDateTime(), metaObject);
    }

    /**
     * 更新时自动填充
     *
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {

        this.setFieldValByName("gmtModified", DateUtil.currentDateTime(), metaObject);
    }

}
