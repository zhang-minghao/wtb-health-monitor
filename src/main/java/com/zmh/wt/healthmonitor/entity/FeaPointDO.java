package com.zmh.wt.healthmonitor.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @Author MH.Zhang
 * @Classname FeePointBO
 * @Date 2023/2/22 10:36
 */
@Data
@AllArgsConstructor
@Builder
public class FeaPointDO {

    private Double feature1;
    private Double feature2;
    private Double feature3;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp gmtReceived;
}
