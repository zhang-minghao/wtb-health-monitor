package com.zmh.wt.healthmonitor.entity;

/**
 * @Author MH.Zhang
 * @Classname MyComplex
 * @Description 双边频谱元数据
 * @Date 2023/3/28 19:45
 */
public class MyComplex {
    public double i;
    // 虚数
    public double j;

    public MyComplex(double i, double j) {
        this.i = i;
        this.j = j;
    }

    public double getMod() {// 求复数的模
        return Math.sqrt(i * i + j * j);
    }

    public static MyComplex Add(MyComplex a, MyComplex b) {
        return new MyComplex(a.i + b.i, a.j + b.j);
    }

    public static MyComplex Subtract(MyComplex a, MyComplex b) {
        return new MyComplex(a.i - b.i, a.j - b.j);
    }

    public static MyComplex Mul(MyComplex a, MyComplex b) {// 乘法
        return new MyComplex(a.i * b.i - a.j * b.j, a.i * b.j + a.j * b.i);
    }

    public static MyComplex GetW(int k, int N) {
        return new MyComplex(Math.cos(-2 * Math.PI * k / N), Math.sin(-2 * Math.PI * k / N));
    }

    public static MyComplex[] butterfly(MyComplex a, MyComplex b, MyComplex w) {
        return new MyComplex[]{Add(a, Mul(w, b)), Subtract(a, Mul(w, b))};
    }

    public static Double[] toModArray(MyComplex[] complex) {
        Double[] res = new Double[complex.length];
        for (int i = 0; i < complex.length; i++) {
            res[i] = complex[i].getMod();
        }
        return res;
    }
}