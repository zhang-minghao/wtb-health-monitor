package com.zmh.wt.healthmonitor.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.sql.Timestamp;

/**
 * @Author MH.Zhang
 * @Classname RealtimeData
 * @Date 2023/2/20 15:03
 */
@TableName("hm_realtime")
@Data
@ToString
@Builder
public class RealtimeDO {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer windturbine;
    private String windfarm;
    private Integer status;
    private Double feature1;
    private Double feature2;
    private Double feature3;

    private Timestamp gmtReceived;
}
