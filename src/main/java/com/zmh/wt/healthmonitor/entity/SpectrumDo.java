package com.zmh.wt.healthmonitor.entity;

/**
 * @Author MH.Zhang
 * @Classname SpectrumDo
 * @Description 用于保存频谱计算的结果
 * @Date 2023/3/28 16:46
 */
public class SpectrumDo {

    /**
     * 频率分辨率
     */
    private double frequencyResolution;

    /**
     * 保存振幅信息,长度是参与傅里叶变换的长度,不是采样点的长度
     */
    private double[] amplitude;

    public double getFrequencyResolution() {
        return frequencyResolution;
    }

    public void setFrequencyResolution(double frequencyResolution) {
        this.frequencyResolution = frequencyResolution;
    }

    public double[] getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(double[] amplitude) {
        this.amplitude = amplitude;
    }
}