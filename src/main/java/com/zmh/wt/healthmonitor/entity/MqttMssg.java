package com.zmh.wt.healthmonitor.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MqttMssg {
    //Mqtt收到的消息格式
    private String id;
    private int cyclecount;
    private int state;
    private int faultcount;
    private double cycle;
    private double feature1;
    private double feature2;
    private double feature3;
    private Timestamp sendtime;
}
