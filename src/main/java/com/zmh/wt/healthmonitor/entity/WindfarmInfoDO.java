package com.zmh.wt.healthmonitor.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Author MH.Zhang
 * @Classname WindfarmDO
 * @Date 2023/2/20 15:22
 */
@TableName("hm_windfarm_info")
@Data
public class WindfarmInfoDO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String windfarm;
    private String name;
    private Integer windturbineCount;
    private String province;
    private Integer region;
    private Integer unconnectedCount;
    private Integer faultCount;
}
