package com.zmh.wt.healthmonitor.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Author MH.Zhang
 * @Classname User
 * @Date 2023/2/9 21:29
 */
@TableName("hm_user")
@Data
public class UserDO {

    @TableId(type = IdType.AUTO)
    public Integer id;
    public String name;
    public String tel;
    public String sex;
    public Integer age;
    public String address;
    public String user;
    public String pwd;
    public Integer position;

}
