package com.zmh.wt.healthmonitor.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @Author MH.Zhang
 * @Classname TxtFileDo
 * @Date 2023/3/27 22:58
 */
@Data
@Builder
public class TxtFileDo {
    private Double[] data;
    private Double max;
    private Double min;
}
