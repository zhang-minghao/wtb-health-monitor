package com.zmh.wt.healthmonitor.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @Author MH.Zhang
 * @Classname WindturbineInfoDO
 * @Date 2023/2/23 15:17
 */

@Data
@TableName("hm_windturbine_info")
public class WindturbineInfoDO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer windturbine;
    private String windfarm;
    private Integer status;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private Timestamp gmtModified;

}
