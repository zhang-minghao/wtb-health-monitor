package com.zmh.wt.healthmonitor.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author MH.Zhang
 * @Classname DateUtils
 * @Date 2023/2/21 11:12
 */
public class DateUtil {
    public static void main(String[] args) {

        System.out.println(currentDateTimeMillis());
        System.out.println(currentDateTime());
    }

    /**
     * 获取当前日期时间 2023-02-23 10:28:09.25
     *
     * @return
     */
    public static Timestamp currentDateTimeMillis() {
        Calendar calendar = Calendar.getInstance();
        return new Timestamp(calendar.getTimeInMillis());
    }

    /**
     * 获取当前日期时间 2023-02-23 10:28:09.268
     *
     * @return
     */
    public static Timestamp currentDateTime() {
        Date dt = new Date();
        return new Timestamp(dt.getTime());
    }
}
