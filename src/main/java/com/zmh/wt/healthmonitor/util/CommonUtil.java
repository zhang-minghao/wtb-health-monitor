package com.zmh.wt.healthmonitor.util;

import java.util.Collection;
import java.util.Map;

/**
 * @Author MH.Zhang
 * @Classname CommomService
 * @Date 2023/2/24 15:16
 */
public class CommonUtil {

    public static boolean isEmpty(Object inputPara) {
        if (inputPara == null) {
            return true;
        }
        if ((inputPara instanceof String)) {
            return ((String) inputPara).trim().equals("");
        } else if (inputPara instanceof Map) {
            return ((Map) inputPara).isEmpty();
        } else if (inputPara instanceof Object[]) {
            Object[] object = (Object[]) inputPara;
            if (object.length == 0) {
                return true;
            }
        } else if (inputPara instanceof Collection) {
            return ((Collection) inputPara).isEmpty();
        } else if (inputPara instanceof CharSequence) {
            return ((CharSequence) inputPara).length() == 0;
        }
        return false;
    }

    /**
     * 降采样
     * @param orgData
     * @param ratio 原始/降采样后比率
     * @return
     */
    public static Double[] downsample(Double[] orgData, int ratio){
        int len = orgData.length/ratio;
        Double[] sampleData = new Double[len];
        int pointer = 0;
        int step = ratio;
        for (int i = 0; i < len; i++) {
            sampleData[i] = orgData[pointer];
            pointer += step;
        }
        return sampleData;
    }

    public static double[] downsample(double[] orgData, int ratio) {
        int len = orgData.length/ratio;
        double[] sampleData = new double[len];
        int pointer = 0;
        int step = ratio;
        for (int i = 0; i < len; i++) {
            sampleData[i] = orgData[pointer];
            pointer += step;
        }
        return sampleData;
    }
}
