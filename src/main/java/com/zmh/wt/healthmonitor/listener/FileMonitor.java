package com.zmh.wt.healthmonitor.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class FileMonitor {

    /**
     * 监控目录的所在路径
     */
    @Value("${realtime.ftp.rootDir}")
    String rootDir;

    @PostConstruct
    public void initFileMonitor() {
        // 监控目录的所在路径
//        String rootDir = "/home/ftpcenter";
        // 轮询间隔 5 秒
        Integer time = 5;
        long interval = TimeUnit.SECONDS.toMillis(time);
        // 创建一个文件观察器用于处理文件的格式, 只上传以txt结尾的数据文件
        //过滤文件格式
        FileAlterationObserver observer = new FileAlterationObserver(
                rootDir,
                FileFilterUtils.and(FileFilterUtils.fileFileFilter(), FileFilterUtils.suffixFileFilter(".txt")),
                null);
//        FileAlterationObserver observer = new FileAlterationObserver(rootDir);
        //设置文件变化监听器
        observer.addListener(new FileListener());
        //创建文件变化监听器
        FileAlterationMonitor monitor = new FileAlterationMonitor(interval, observer);
        // 开始监控
        try {
            monitor.start();
            log.info("File Listener is started");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
