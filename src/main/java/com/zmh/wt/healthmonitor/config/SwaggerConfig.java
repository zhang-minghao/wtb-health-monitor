package com.zmh.wt.healthmonitor.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author MyungHo
 * @apiNote 轻量级的Swagger生成Api文档
 * @date 2021/10/28 11:15
 */
@Configuration
@EnableOpenApi
@EnableKnife4j
public class SwaggerConfig {

    /**
     * api的主页显示信息
     */
    private static ApiInfo apiInfo;

    static {
        apiInfo = new ApiInfoBuilder()
                .title("企业用户API接口")
                .description("API接口文档")
                .termsOfServiceUrl("http://www.bupt.edu.cn")
                .contact(new Contact("zmh", "xxx.com", "321991001@qq.com"))
                .version("1.0")
                .build();
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                // 配置分组名
                .groupName("联调使用")
                .apiInfo(apiInfo)
                .select()
                // 设置扫描包的地址 : com.hanliy.controller
                .apis(RequestHandlerSelectors.basePackage("com.zmh.wt.healthmonitor.controller"))
                // 设置路径筛选 只扫描com.hanliy.controller/test/下面的包
                // .paths(PathSelectors.ant("/test/**"))
                // com.hanliy.controller下的任何接口信息
                .paths(PathSelectors.any())
                .build();
    }
}

