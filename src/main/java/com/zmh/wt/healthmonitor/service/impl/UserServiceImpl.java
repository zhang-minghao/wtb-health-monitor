package com.zmh.wt.healthmonitor.service.impl;

import com.zmh.wt.healthmonitor.common.Result;
import com.zmh.wt.healthmonitor.constant.Constants;
import com.zmh.wt.healthmonitor.entity.UserDO;
import com.zmh.wt.healthmonitor.mapper.UserMapper;
import com.zmh.wt.healthmonitor.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Author MH.Zhang
 * @Classname UserService
 * @Date 2023/2/9 21:44
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    UserMapper userMapper;

    /**
     * 存储用户数据
     *
     * @param userDO
     * @return
     */
    @Override
    public boolean save(UserDO userDO) {

        if (!userIsAvailable(userDO)){
            return false;
        }

        int insert = userMapper.insert(userDO);
        if (insert <= Constants.ZERO){
            return false;
        }

        return true;
    }

    /**
     * 查询所有用户信息
     *
     * @return
     */
    @Override
    public Result<List<UserDO>> searchAllUser() {
        List<UserDO> userList = userMapper.searchAll();
        if (userList.size() == 0) {
            return Result.buildResult(Result.Status.NOT_FOUND, "用户表为空");
        }
        return Result.buildResult(Result.Status.SUCCESS, userList);
    }

    /**
     * 密码校验
     *
     * @param user
     * @param pwd
     * @return
     */
    @Override
    public Boolean checkPwd(String user, String pwd) {
        String realPwd = userMapper.queryPwd(user);
        if (!realPwd.equals(pwd)){
            return false;
        }
        return true;
    }

    @Override
    public Boolean queryUser(String user) {
        String realuser = userMapper.queryUser(user);
        if (realuser == null){
            return false;
        }
        return true;
    }
    /**
     * 用户删除
     *
     * @param user
     * @return
     */
//    @Override
//    public Boolean deleteUser(String user) {
//        String originUser = userMapper.queryUser(user);
//        if(originUser == null){
//            return false;
//        }
//        userMapper.deleteUser(user);
//        String afterUser = userMapper.queryUser(user);
//        if(afterUser == null){
//            return true;
//        }else{
//            return false;
//        }
//    }

    @Override
    public Boolean deleteUser(String user) {
        String originUser = userMapper.queryUser(user);
        if(originUser == null){
            return false;
        }
        userMapper.deleteUser(user);
        String afterUser = userMapper.queryUser(user);
        if(afterUser == null){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 新用户合法检验
     * @param userDO
     * @return
     */
    private Boolean userIsAvailable(UserDO userDO){
        String user = userDO.getUser();
        if (Objects.isNull(user) || "".equals(user)){
            return false;
        }

        String pwd = userDO.getPwd();
        if (Objects.isNull(pwd) || "".equals(pwd)){
            return false;
        }

        Integer num = userMapper.countUser(user);
        if (num >= Constants.ONE){
            return false;
        }

        return true;
    }
}
