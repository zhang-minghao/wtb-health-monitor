package com.zmh.wt.healthmonitor.service;

import java.util.Map;

/**
 * @Author MH.Zhang
 * @Classname WindturbineService
 * @Date 2023/2/23 16:30
 */
public interface WindturbineService {

    /**
     * 查询单个风机的状态
     *
     * @param windfarm
     * @param windturbine
     * @return
     */
    public Integer queryWindturbineStatus(String windfarm, Integer windturbine);

    /**
     * 查询数据库所有风机状态(正常-故障)
     *
     * @param windfarm
     * @return
     */
    public Map<String, Integer> queryDbAllStatus(String windfarm);

    /**
     * 查询所有风机状态(正常-故障-未连接)
     *
     * @param windfarm
     * @return
     */
    public Map<String, Integer> queryAllStatContainsUnconnected(String windfarm);


    /**
     * 更新缓存的风机状态
     *
     * @param windfarm
     * @param unconnectedWTId
     * @param status
     * @return
     */
    public Integer updateWindturbineCacheStatus(String windfarm, Integer unconnectedWTId, Integer status);

}
