package com.zmh.wt.healthmonitor.service;

import com.zmh.wt.healthmonitor.common.Result;
import com.zmh.wt.healthmonitor.entity.RealtimeDO;
import com.zmh.wt.healthmonitor.entity.SpectrumDo;

import java.io.IOException;
import java.util.List;

/**
 * @Author MH.Zhang
 * @Classname RealTimeService
 * @Date 2023/2/20 16:10
 */
public interface RealTimeService {

    /**
     * 新实时数据（Mqtt消息触发）
     *
     * @param realtimeDO
     * @return
     */
    public Result insertRealtimeData(RealtimeDO realtimeDO);

    /**
     * 更新当前风场的特征曲线数据
     *
     * @param realtimeDO
     * @return
     */
    public Integer updateFeaCurve(RealtimeDO realtimeDO);

    /**
     * 获取风机的最新实时数据
     *
     * @param windfarm
     * @param windturbine
     * @return
     */
    public Result getFeaCurve(String windfarm, Integer windturbine);

    /**
     * 当前风场最大风机编号
     *
     * @param windfarm
     * @return
     */
    public Integer getMaxWindturbineId(String windfarm);

    /**
     * 当前监测风机数量
     *
     * @param windfarm
     * @return
     */
    public Integer getWindturbineNum(String windfarm);


    /**
     * 获取最新wav文件的绝对路径（滤波）
     *
     * @return
     * @throws IOException
     */
    public Result getLatestWavAbsolutePath() throws IOException;

    /**
     * 获取最新txt文件的绝对路径（滤波）
     *
     * @return
     * @throws IOException
     */
    public Result getLatestTxtAbsolutePath() throws IOException;

    /**
     * 获取最新txt文件的绝对路径（原始数据，无滤波）
     * @return
     */
    public Result getLatestOrigTxtAbsolutePath();

    /**
     * 获取最新文件的频谱数据（滤波）
     *
     * @return
     */
    public Result getLastFileSpectrum();

    /**
     * 查询风场最新N条风机实时数据记录
     * @return
     */
    public Result queryWindFarmLastRecord(String windfarm, Integer N);

    /**
     * 查询风场某状态下最新N条风机实时数据记录
     * @return
     */
    public Result queryWindFarmLastRecordByStatus(String windfarm, Integer status, Integer N);


    /**
     * 获取最新滤波前txt文件20万点（入参：风机id） 11
     *
     * @return
     */
    public Double[] getLatestOrigTxtData(String windturbine);

    /**
     * 获取最新滤波后txt文件20万点 11
     *
     * @return
     */
    public Double[] getLatestTxtDataAfterFiltering(String windturbine);

    /**
     * 频谱txt文件20万点
     *
     * @return
     */
    public SpectrumDo getLatestTxtSpectrumData(String windturbine);

    /**
     * 分段频谱20组 22
     *
     * @param windturbine
     * @return
     */
    public List<double[]> getLatestTxtSpectrumCollection(String windturbine);

}
