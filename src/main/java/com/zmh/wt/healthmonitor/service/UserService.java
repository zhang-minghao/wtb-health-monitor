package com.zmh.wt.healthmonitor.service;

import com.zmh.wt.healthmonitor.common.Result;
import com.zmh.wt.healthmonitor.entity.UserDO;
import org.apache.ibatis.annotations.Param;

/**
 * @author MyungHo
 */
public interface UserService {

    /**
     * 存储用户数据
     *
     * @param userDO
     * @return
     */
    public boolean save(UserDO userDO);

    /**
     * 查询所有用户信息
     *
     * @return
     */
    public Result searchAllUser();

    /**
     * 密码校验
     * @param user
     * @param pwd
     * @return
     */
    public Boolean checkPwd(String user, String pwd);

    public Boolean queryUser(String user);

    public Boolean deleteUser(String user);
}
