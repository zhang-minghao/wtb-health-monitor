package com.zmh.wt.healthmonitor.constant;

/**
 * @Author MH.Zhang
 * @Classname CacheConstant
 * @Description 缓存常量
 * @Date 2023/2/20 21:15
 */
public class CacheConstant {

    /**
     * 业务相关key前缀
     */
    public static final String KEY_REAL_TIME = "real_time";
    private final static String WT_STATUS = "wt_status";

    public static final String KEY_PLAY_BACK = "play_back";
    public static final String KEY_WIND_FARM = "wind_farm";
    public static final String KEY_WIND_TURBINE = "wind_turbine";

    /**
     * 缓存key连接符
     */
    public static final String CONNECTOR = "_";

    /**
     * 缓存后缀
     */
    private final static String LATEST_FILE = "latest_file";

    /**
     * 风机最新文件key生成
     * @param windturbine
     * @return
     */
    public static String getLatestFileKeyById(String windturbine){
        return getKey(KEY_REAL_TIME, "wt" + windturbine, LATEST_FILE);
    }

    /**
     * 获取风机状态key
     * @param windfarm
     * @param windturbine
     * @return
     */
    public static String getWtStatusKey(String windfarm, String windturbine){
        return getKey(KEY_WIND_TURBINE, WT_STATUS, windfarm + CONNECTOR + windturbine);
    }


    /**
     * 组合三段key
     *
     * @param profix
     * @param mid
     * @param suffix
     * @return
     */
    public static String getKey(String profix, String mid, String suffix) {
        return new StringBuilder().
                append(profix).append(CONNECTOR).
                append(mid).append(CONNECTOR).
                append(suffix).
                toString();
    }

    /**
     * 组合二段key
     *
     * @param profix
     * @param suffix
     * @return
     */
    public static String getKey(String profix, String suffix) {
        return new StringBuilder().
                append(profix).append(CONNECTOR).
                append(suffix).
                toString();
    }
}
