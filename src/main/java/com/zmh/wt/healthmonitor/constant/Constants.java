package com.zmh.wt.healthmonitor.constant;

/**
 * @Author MH.Zhang
 * @Classname CommonConstant
 * @Description 常量定义
 * @Date 2023/2/22 11:12
 */
public class Constants {
    /**
     * 数字
     */
    public static final Integer ZERO = 0;
    public static final Integer ONE = 1;
    public static final Integer TWO = 2;
    public static final Integer TEN = 10;
    public static final Integer HUNDRED = 100;

    /**
     * 常用Integer状态码
     */
    public static final Integer SUCCESS_INT = 1;
    public static final Integer FAIL_INT = 0;

    /**
     * 风机运行状态
     */
    public static final Integer UNCONNECTED = 9;
    public static final Integer CONNECTED = 2;
    public static final Integer NORMAL = 0;
    public static final Integer FAULT = 1;
}
