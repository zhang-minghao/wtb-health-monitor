package com.zmh.wt.healthmonitor.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zmh.wt.healthmonitor.common.Result;
import com.zmh.wt.healthmonitor.entity.*;
import com.zmh.wt.healthmonitor.mapper.RealtimeMapper;
import com.zmh.wt.healthmonitor.mapper.UserMapper;
import com.zmh.wt.healthmonitor.mapper.WindfarmMapper;
import com.zmh.wt.healthmonitor.mapper.WindturbineMapper;
import com.zmh.wt.healthmonitor.service.RealTimeService;
import com.zmh.wt.healthmonitor.service.UserService;
import com.zmh.wt.healthmonitor.service.WindturbineService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author MH.Zhang
 * @Classname UserController
 * @Description 测试
 * @Date 2023/2/9 21:23
 */
@CrossOrigin
@RestController
@Api(tags = "测试相关")
@RequestMapping("/test")
public class TestController {


    @Resource
    RealTimeService realTimeService;
    @Resource
    WindturbineService windturbineService;
    @Resource
    RealtimeMapper realtimeMapper;
    @Resource
    WindfarmMapper windfarmMapper;
    @Resource
    WindturbineMapper windturbineMapper;

    @Value("${realtime.ftp.rootDir}")
    String rootDir;

    /**
     * 获取系统当前日期时间
     *
     * @return
     */
    @ApiOperation("获取系统当前日期时间")
    @GetMapping("/getSystemDate")
    public String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        String date1 = formatter.format(date);

        date = new Date();
        formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String date2 = formatter.format(date);

        return date1 + " / " + date2;
    }


    /**
     * 实时数据分页查询
     *
     * @param current
     * @param size
     * @return
     */
    @ApiOperation("实时数据分页查询")
    @GetMapping("searchRealtimeData")
    public Result<RealtimeDO> searchRealtimeData(int current, int size) {
        Page<RealtimeDO> page = new Page<>(current, size);
        realtimeMapper.selectPage(page, null);
        List<RealtimeDO> list = page.getRecords();
        Result result = Result.buildResult(Result.Status.SUCCESS, list);
        return result;
    }

    /**
     * 新建实时数据实体测试（mp）
     *
     * @param realtimeDO
     * @return
     */
    @ApiOperation("新建实时数据实体测试（mp）")
    @PostMapping("/crateRealtimeDO")
    public Result crateRealtimeDO(@RequestBody RealtimeDO realtimeDO) {

        System.out.println(realtimeDO.toString());
        realtimeMapper.insert(realtimeDO);
        return Result.buildResult(Result.Status.SUCCESS, realtimeDO);
    }

    /**
     * 风场信息分页查询
     *
     * @param current
     * @param size
     * @return
     */
    @ApiOperation("风场信息分页查询")
    @GetMapping("/searchWindfarmInfo")
    public Result<WindfarmInfoDO> searchWindfarmInfo(int current, int size) {
        Page<WindfarmInfoDO> page = new Page<>(current, size);
        windfarmMapper.selectPage(page, null);
        List<WindfarmInfoDO> list = page.getRecords();
        Result result = Result.buildResult(Result.Status.SUCCESS, list);
        return result;
    }

    /**
     * 风机信息分页查询
     *
     * @param current
     * @param size
     * @return
     */
    @ApiOperation("风机信息分页查询")
    @GetMapping("/searchWindturbineInfo")
    public Result<WindturbineInfoDO> searchWindturbineInfo(int current, int size) {
        Page<WindturbineInfoDO> page = new Page<>(current, size);
        windturbineMapper.selectPage(page, null);
        List<WindturbineInfoDO> list = page.getRecords();
        Result result = Result.buildResult(Result.Status.SUCCESS, list);
        return result;
    }

    /**
     * 插入一条实时数据
     *
     * @param realtimeDO
     * @return
     */
    @ApiOperation("插入一条实时数据")
    @PostMapping("/insertRealtimeData")
    public Result insertRealtimeData(@RequestBody RealtimeDO realtimeDO) {
        realtimeMapper.insert(realtimeDO);
        return Result.buildResult(Result.Status.SUCCESS);
    }

    /**
     * 更新特征曲线
     *
     * @param realtimeDO
     * @return
     */
    @ApiOperation("更新特征曲线")
    @PostMapping("/updateFeaCurve")
    public Result updateFeaCurve(@RequestBody RealtimeDO realtimeDO) {
        realTimeService.updateFeaCurve(realtimeDO);
        return Result.buildResult(Result.Status.SUCCESS);
    }

    /**
     * 从数据库读最新特征曲线
     *
     * @param windfarm
     * @param windturbine
     * @return
     */
    @ApiOperation("从数据库读最新特征曲线")
    @GetMapping("/getLastFeaCurve")
    public Result getLastFeaCurve(String windfarm, Integer windturbine, Integer N) {
        LinkedList<FeaPointDO> feaCurve = realtimeMapper.queryLastNRecord(windfarm, windturbine, N);
        return Result.buildResult(Result.Status.SUCCESS, feaCurve);
    }

    /**
     * 查询数据库所有风机的健康状态
     *
     * @param windfarm
     * @return
     */
    @ApiOperation("查询数据库所有风机的健康状态")
    @GetMapping("/queryAllWindturbineStatus")
    public Result queryAllWindturbineStatus(String windfarm) {
        Map<String, Integer> stringObjectMap = windturbineService.queryDbAllStatus(windfarm);
        return Result.buildResult(Result.Status.SUCCESS, stringObjectMap);
    }

    /**
     * 更新风机在线状态为未连接
     *
     * @return
     */
    @ApiOperation("更新风机在线状态为未连接")
    @GetMapping("/updateWindturbineStatus")
    public String updateWindturbineStatus() {
        windturbineService.updateWindturbineCacheStatus("10001", 1, 9);
        return "ok";
    }

    /**
     * 启动检测
     *
     * @return
     */
    @ApiOperation("启动检测")
    @GetMapping("/isAlive")
    public String test() {
        return "yes";
    }
}
