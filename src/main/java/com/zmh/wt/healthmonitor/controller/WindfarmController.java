package com.zmh.wt.healthmonitor.controller;

import com.zmh.wt.healthmonitor.service.RealTimeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author MH.Zhang
 * @Classname WindfarmController
 * @Description 风场相关
 * @Date 2023/2/20 15:10
 */
@CrossOrigin
@Api(tags = "风场信息")
@RestController
public class WindfarmController {

    @Resource
    RealTimeService realTimeService;

    /**
     * 获取风场最大风机id
     *
     * @return
     */
    @ApiOperation("获取风场最大风机id")
    @GetMapping("/searchMaxWindturbineId")
    public Integer searchMaxWindturbineId(String windfarm) {
        return realTimeService.getMaxWindturbineId(windfarm);
    }

}
