package com.zmh.wt.healthmonitor.controller;

import com.zmh.wt.healthmonitor.common.Result;
import com.zmh.wt.healthmonitor.entity.RealtimeDO;
import com.zmh.wt.healthmonitor.entity.SpectrumDo;
import com.zmh.wt.healthmonitor.service.RealTimeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.checkerframework.checker.units.qual.A;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Objects;


/**
 * @Author MH.Zhang
 * @Classname RealTimeMonitorController
 * @Description 实时监测
 * @Date 2023/2/20 10:55
 */
@CrossOrigin
@RestController
@Api(tags = "实时监测")
@RequestMapping("/realtime")
public class RealTimeMonitorController {

    @Resource
    RealTimeService realTimeService;

    /**
     * 插入实时监测数据
     *
     * @param realtimeDO
     * @return
     */
    @ApiOperation("插入实时监测数据")
    @PostMapping("/insertRealtimeData")
    public Result insertRealtimeData(@RequestBody RealtimeDO realtimeDO) {

        Result result = realTimeService.insertRealtimeData(realtimeDO);
        return result;
    }

    /**
     * 查询最新特征曲线
     *
     * @param windfarm
     * @param windturbine
     * @return
     */
    @ApiOperation(value = "查询最新特征曲线")
    @GetMapping("/quaryLatestFeaCurve")
    public Result queryLatestFeaCurve(String windfarm, Integer windturbine) {
        Result result = realTimeService.getFeaCurve(windfarm, windturbine);
        return result;
    }

    /**
     * 获取最新滤波后wav文件的绝对地址
     *
     * @return
     */
    @ApiOperation("获取最新滤波后wav文件的绝对地址")
    @GetMapping("/getLatestWavAbsolutePath")
    public Result getLatestFileAbsolutePath() throws IOException {
        Result latestWavAbsolutePath = realTimeService.getLatestWavAbsolutePath();
        return latestWavAbsolutePath;
    }

    /**
     * 获取最新文件的[频率分辨率,频谱数据]
     *
     * @return
     */
    @ApiOperation("获取最新检测文件的[频率分辨率,频谱数据]")
    @GetMapping("/getLastFileSpectrum")
    public Result getLastFileSpectrum() {
        Result lastFileSpectrum = realTimeService.getLastFileSpectrum();
        return lastFileSpectrum;
    }

    /**
     * 获取最新滤波后txt文件绝对地址
     *
     * @return
     */
    @ApiOperation("获取最新滤波后txt文件绝对地址")
    @GetMapping("/getLastTxtAbsolutePath")
    public Result getLastTxtAbsolutePath() throws IOException {
        Result latestTxtAbsolutePath = realTimeService.getLatestTxtAbsolutePath();
        return latestTxtAbsolutePath;
    }

    /**
     * 获取最新原始数据txt文件绝对地址
     * @return
     */
    @ApiOperation("获取最新原始数据txt文件绝对地址")
    @GetMapping("/getLatestOrigTxtAbsolutePath")
    public Result getLatestOrigTxtAbsolutePath(){
        Result latestOrigTxtAbsolutePath = realTimeService.getLatestOrigTxtAbsolutePath();
        return latestOrigTxtAbsolutePath;
    }

    /**
     * 获取最新滤波前txt文件20万点（入参：风机id）
     * @param windturbine
     * @return
     */
    @ApiOperation("获取某风机最新原始数据")
    @GetMapping("/getLatestOrigTxtData")
    public Result getLatestOrigTxtData(String windturbine){
        Double[] latestOrigTxtData = realTimeService.getLatestOrigTxtData(windturbine);

        if (Objects.isNull(latestOrigTxtData) || latestOrigTxtData.length == 0){
            return Result.buildResult(Result.Status.NOT_FOUND);
        }

        return Result.buildResult(Result.Status.SUCCESS,"ok",latestOrigTxtData);
    }

    /**
     * 获取最新滤波后txt文件20万点
     *
     * @return
     */
    @ApiOperation("获取某风机最新滤波后数据")
    @GetMapping("/getLatestTxtDataAfterFiltering")
    public Result getLatestTxtDataAfterFiltering(String windturbine){
        Double[] latestTxtDataAfterFiltering = realTimeService.getLatestTxtDataAfterFiltering(windturbine);

        if (Objects.isNull(latestTxtDataAfterFiltering) || latestTxtDataAfterFiltering.length == 0){
            return Result.buildResult(Result.Status.NOT_FOUND);
        }

        return Result.buildResult(Result.Status.SUCCESS,"ok",latestTxtDataAfterFiltering);
    }

    /**
     * 频谱txt文件20万点
     *
     * @return
     */
    @ApiOperation("获取某风机最新频谱数据")
    @GetMapping("/getLatestTxtSpectrumData")
    public Result getLatestTxtSpectrumData(String windturbine){
        SpectrumDo latestTxtSpectrum = realTimeService.getLatestTxtSpectrumData(windturbine);

        if (Objects.isNull(latestTxtSpectrum) || Objects.isNull(latestTxtSpectrum.getAmplitude()) || latestTxtSpectrum.getAmplitude().length == 0){
            return Result.buildResult(Result.Status.NOT_FOUND);
        }

        return Result.buildResult(Result.Status.SUCCESS, "ok", latestTxtSpectrum);
    }

    /**
     * 获取某风机分段频谱数据
     * @param windturbine
     * @return
     */
    @ApiOperation("获取某风机分段频谱数据")
    @GetMapping("/getLatestTxtSpectrumCollection")
    public Result getLatestTxtSpectrumCollection(String windturbine){
        List<double[]> latestTxtSpectrumCollection = realTimeService.getLatestTxtSpectrumCollection(windturbine);

        if (Objects.isNull(latestTxtSpectrumCollection) || latestTxtSpectrumCollection.size() == 0){
            return Result.buildResult(Result.Status.NOT_FOUND);
        }

        return Result.buildResult(Result.Status.SUCCESS, "ok", latestTxtSpectrumCollection);
    }


    /**
     * 获取最新数据列表
     *
     * @return
     */
    @ApiOperation("获取某风场所有风机的最新N条实时数据")
    @GetMapping("/queryWindFarmLastRecord")
    public Result queryWindFarmLastRecord(String windfarm, Integer N){
        Result result = realTimeService.queryWindFarmLastRecord(windfarm, N);
        return result;
    }

    /**
     * 获取某风场（某状态下）所有风机的最新N条实时数据
     *
     * @return
     */
    @ApiOperation("获取某风场（某状态下）所有风机的最新N条实时数据")
    @GetMapping("queryWindFarmLastRecordByStatus")
    public Result queryWindFarmLastRecordByStatus(String windfarm, Integer status, Integer N){
        Result result = realTimeService.queryWindFarmLastRecordByStatus(windfarm, status, N);
        return result;
    }

}
