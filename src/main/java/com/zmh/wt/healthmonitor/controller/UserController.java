package com.zmh.wt.healthmonitor.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zmh.wt.healthmonitor.common.Result;
import com.zmh.wt.healthmonitor.entity.UserDO;
import com.zmh.wt.healthmonitor.mapper.UserMapper;
import com.zmh.wt.healthmonitor.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author AAA
 * @Classname UserController
 * @Description TODO 登录需要改进加密，token等，诶，算了，xxxx
 * @Date 2023/5/10 15:04
 */
@CrossOrigin
@RestController
@Api(tags = "用户管理")
@RequestMapping("/user")
public class UserController {

    @Resource
    UserService userService;

    @Resource
    UserMapper userMapper;

    /**
     * 新建用户
     *
     * @param userDO
     * @return
     */
    @ApiOperation("新建用户")
    @PostMapping("/createNewUser")
    public Result save(@RequestBody UserDO userDO) {

        boolean successFlag = userService.save(userDO);

        if (!successFlag) {
            return Result.buildResult(Result.Status.ERROR, "用户已存在或信息错误",false);
        }

        return Result.buildResult(Result.Status.SUCCESS, "新用户创建成功",true);

    }

    @ApiOperation("用户登录")
    @PostMapping("/login")
    public Result login(@RequestBody UserDO userDo) {
        String user = userDo.getUser();
        String pwd = userDo.getPwd();
        Boolean isUserExist = userService.queryUser(user);
        if(!isUserExist){
            return Result.buildResult(Result.Status.UNAUTHORIZED,"用户不存在",false);
        }
        Boolean loginSuccess = userService.checkPwd(user, pwd);
        if (!loginSuccess) {
            return Result.buildResult(Result.Status.PWD_ERROR, "密码错误",false);
        }

        return Result.buildResult(Result.Status.SUCCESS, "密码校验通过",true);

    }

    /**
     * 查询所有用户
     *
     * @return
     */
    @ApiOperation("查询所有用户")
    @GetMapping("/searchAllUser")
    public Result<UserDO> searchAllUser() {

        Result result = userService.searchAllUser();
        return result;
    }

    /**
     * 分页查询用户数据
     *
     * @param current
     * @param size
     * @return
     */
    @ApiOperation("分页查询用户数据")
    @GetMapping("/searchUser")
    public Result<UserDO> searchUser(int current, int size) {
        Page<UserDO> page = new Page<>(current, size);
        userMapper.selectPage(page, null);
        List<UserDO> list = page.getRecords();
        Result result = Result.buildResult(Result.Status.SUCCESS, list);
        return result;

    }
    /**
     * 删除数据
     * @param user
     * @return
     */
    @ApiOperation("删除用户数据")
    @GetMapping("/deleteUser")
    public Result deleteUser(String user){
        Boolean isDelete = userService.deleteUser(user);
        if (!isDelete) {
            return Result.buildResult(Result.Status.PWD_ERROR, "删除失败",false);
        }

        return Result.buildResult(Result.Status.SUCCESS, "删除成功",true);


    }
}
