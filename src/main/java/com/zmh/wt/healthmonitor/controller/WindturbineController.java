package com.zmh.wt.healthmonitor.controller;

import com.zmh.wt.healthmonitor.common.Result;
import com.zmh.wt.healthmonitor.service.WindturbineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Author MH.Zhang
 * @Classname WindturbineController
 * @Description 风机信息相关
 * @Date 2023/2/27 10:18
 */
@CrossOrigin
@RestController
@Api(tags = "风机信息")
@RequestMapping("/windturbine")
public class WindturbineController {

    @Resource
    WindturbineService windturbineService;

    /**
     * 查询数据库所有风机的健康状态
     *
     * @param windfarm
     * @return
     */
    @ApiOperation("查询数据库所有风机的健康状态(0-正常 1-故障 9-未连接)")
    @GetMapping("/queryAllWindturbineStatus")
    public Result queryAllWindturbineStatus(String windfarm) {
        Map<String, Integer> stringObjectMap = windturbineService.queryAllStatContainsUnconnected(windfarm);
        return Result.buildResult(Result.Status.SUCCESS, stringObjectMap);
    }
}
