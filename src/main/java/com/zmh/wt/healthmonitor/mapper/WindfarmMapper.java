package com.zmh.wt.healthmonitor.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmh.wt.healthmonitor.entity.WindfarmInfoDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author MH.Zhang
 * @Classname WindfarmMapper
 * @Date 2023/2/20 16:16
 */
@Mapper
public interface WindfarmMapper extends BaseMapper<WindfarmInfoDO> {
}
