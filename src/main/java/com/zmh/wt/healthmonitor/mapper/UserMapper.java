package com.zmh.wt.healthmonitor.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmh.wt.healthmonitor.entity.UserDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author MH.Zhang
 * @Classname UserMapper
 * @Date 2023/2/9 21:43
 */
@Mapper
public interface UserMapper extends BaseMapper<UserDO> {
    /**
     * 查询所有用户--mybatis示例
     *
     * @return
     */
    public List<UserDO> searchAll();

    /**
     * 查询用户数
     * @param user
     * @return
     */
    public Integer countUser(@Param("user") String user);

    /**
     * 查询密码
     * @param user
     * @return
     */
    public String queryPwd(@Param("user") String user);

    public String queryUser (@Param("user") String user);

    public void deleteUser(@Param("user") String user);
}
