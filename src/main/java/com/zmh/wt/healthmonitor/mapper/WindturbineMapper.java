package com.zmh.wt.healthmonitor.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmh.wt.healthmonitor.entity.WindturbineInfoDO;
import com.zmh.wt.healthmonitor.mapper.Handler.MapResultHander;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author MH.Zhang
 * @Classname WindturbineMapper
 * @Date 2023/2/22 10:46
 */
public interface WindturbineMapper extends BaseMapper<WindturbineInfoDO> {

    /**
     * 查询某风场风机数量
     *
     * @param windfarm
     * @return
     */
    public Integer queryWindturbineCount(@Param("windfarm") String windfarm);

    /**
     * 查询某风场某状态(正常-故障)的风机数量
     *
     * @param windfarm
     * @param status
     * @return
     */
    public Integer queryStatusCount(@Param("windfarm") String windfarm, @Param("status") Integer status);

    /**
     * 查询某风场某状态风机编号列表
     *
     * @param windfarm
     * @param status
     * @return
     */
    public List<Integer> queryStatusList(@Param("windfarm") String windfarm, @Param("status") Integer status);

    /**
     * 查询某风场所有风机编号列表
     *
     * @param windfarm
     * @return
     */
    public List<Integer> queryWTList(@Param("windfarm") String windfarm);


    /**
     * 查询风场所有风机健康状态Map
     *
     * @param windfarm
     * @param mapResultHander
     * @attention 忽略爆红，此处为特殊处理
     */
    public void queryAllWindturbineStatus(@Param("windfarm") String windfarm, MapResultHander mapResultHander);
}
