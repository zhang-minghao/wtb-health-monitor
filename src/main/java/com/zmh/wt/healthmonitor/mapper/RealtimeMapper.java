package com.zmh.wt.healthmonitor.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmh.wt.healthmonitor.entity.FeaPointDO;
import com.zmh.wt.healthmonitor.entity.RealtimeDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.LinkedList;
import java.util.List;

/**
 * @author MyungHo
 */
@Mapper
public interface RealtimeMapper extends BaseMapper<RealtimeDO> {
    /**
     * 查询最大风机编号
     *
     * @param windfarmId
     * @return
     */
    public Integer searchMaxWindturbineId(@Param("windfarmId") String windfarmId);

    /**
     * 查询最新风机N条实时数据记录
     *
     * @param windfarm
     * @param windturbine
     * @param N
     * @return
     */
    public LinkedList<FeaPointDO> queryLastNRecord(@Param("windfarm") String windfarm, @Param("windturbine") Integer windturbine, @Param("N") Integer N);

    /**
     * 查询风场最新N条风机实时数据记录
     * @param windfarm
     * @param N
     * @return
     */
    public List<RealtimeDO> queryWindFarmLastRecord(@Param("windfarm") String windfarm, @Param("N") Integer N);

    /**
     * 查询风场某状态下最新N条风机实时数据记录
     * @param windfarm
     * @param status
     * @param N
     * @return
     */
    public List<RealtimeDO> queryWindFarmLastRecordByStatus(@Param("windfarm") String windfarm, @Param("status") Integer status, @Param("N") Integer N);
}
