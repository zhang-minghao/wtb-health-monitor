package com.zmh.wt.healthmonitor.mapper.Handler;

import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author MH.Zhang
 * @Classname ResultHander
 * @Description Map<String, Object>查询
 * @Date 2023/2/23 20:03
 */
@SuppressWarnings("all")
public class MapResultHander implements ResultHandler<Map<String, Object>> {

    private final Map<String, Object> mapResults = new HashMap<>();
    private final String key, val;

    @Override
    public void handleResult(ResultContext<? extends Map<String, Object>> resultContext) {
        Map map = (Map) resultContext.getResultObject();
        mapResults.put(map.get(key).toString(), map.get(val));
    }

    public MapResultHander(String key, String val) {
        this.key = key;
        this.val = val;
    }

    public Map getMapResults() {
        return mapResults;
    }
}
