package com.zmh.wt.healthmonitor.bo;

import com.zmh.wt.healthmonitor.constant.Constants;
import com.zmh.wt.healthmonitor.entity.FeaPointDO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Queue;

/**
 * @Author MH.Zhang
 * @Classname FeaCurveBO
 * @Description 特征曲线存储
 * @Date 2023/2/22 10:36
 */
@Data
@AllArgsConstructor
@Builder
public class FeaCurveBO {

    private Integer capacity;
    private Integer windturbine;
    private String windfarm;
    private Queue<FeaPointDO> feePoints;

    /**
     * 新增特征点
     *
     * @param feaPointDO NEED capacity怎么确定的
     * @return
     */
    public Integer addFeePoint(FeaPointDO feaPointDO) {
        if (null == capacity || capacity.equals(0)) {
            return Constants.FAIL_INT;
        }

        if (feePoints.size() >= capacity) {
            feePoints.poll();
        }

        feePoints.offer(feaPointDO);
        return Constants.SUCCESS_INT;
    }
}
