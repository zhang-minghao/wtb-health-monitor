package com.zmh.wt.healthmonitor.utils;

import com.zmh.wt.healthmonitor.entity.TxtFileDo;
import com.zmh.wt.healthmonitor.util.CommonUtil;
import jdk.nashorn.internal.runtime.linker.InvokeByName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author MH.Zhang
 * @Classname TestUtil
 * @Date 2023/3/27 11:30
 */
public class TestUtil {
    public static void main(String[] args) {

        Integer a = 1;
        String b = "1";
        System.out.println(b.equals(a.toString()));

        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        for (int i = 0; i < list.size(); i++){
            if (list.get(i).equals("a")){
                list.remove(list.get(i));
            }
        }

//        String path = "D:\\ftpCenter";
//        System.out.println(path);
//
//        System.out.println(System.getProperty("user.dir"));
    }

    @Test
    public void logTest(){

        TxtFileDo txtFileDo = TxtUtil.readTxtByPath("D:\\ftpCenter\\HH_WF10001_WT7_CH1_20000212041999.txt");
        Double[] origData = Arrays.copyOfRange(txtFileDo.getData(), 0, 200000);

        TxtFileDo highpass = TxtUtil.highpass(origData);


//        Double[] resData  = CommonUtil.downsample(highpass.getData(), 50);
        Double[] resData  = CommonUtil.downsample(origData, 50);

        Double[] doubles = Arrays.stream(resData).map(org->20 * Math.log(Math.abs(org / 10.0)+10e-20) * (org >= 0 ? 1:-1)).collect(Collectors.toList()).toArray(new Double[0]);
        for (int i = 0; i < doubles.length; i++) {
            System.out.println(i + " - " + resData[i] + " - " + doubles[i]);
        }
    }

    @Test
    public void Test(){
        Double org = 0.0;
        double res = 20 * Math.log(Math.abs(org / 10.0)+10e-20) * (org >= 0 ? 1:-1);
        System.out.println(res);
        System.out.println(1e-9);
    }
}
