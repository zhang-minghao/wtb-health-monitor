#项目说明  

##环境配置   
1.保证pom所有依赖拉取成功  
2.少量参数配置于application.properties   

##MQTT数据格式   
1;6;1;2;1;1.5;1.5;1.5  
id; cyclecount; state; faultcount; cycle; feature1; feature2; feature3;  

##文件名格式
HH_WF10001_WT1_CH1_20210410000000.txt
风场名称_WF风场编号_WT风机编号_CH通道号_日期.txt

##文件名格式（修改 2023/5/11）
HH_WF10001_WT1_CH1_ST1_CY1_DT20210410000000.txt
风场名称_WF风场编号_WT风机编号_CH通道号_ST状态_CY周期数_DT日期.txt
ST-状态：0正常，1故障；CY-周期数：0或1

##接口文档访问地址  
http://localhost:8080/doc.html#/home  

##项目临时地址  
https://gitee.com/zhang-minghao/wtb-health-monitor/tree/master

##待改进内容  
异步处理  
动态配置  